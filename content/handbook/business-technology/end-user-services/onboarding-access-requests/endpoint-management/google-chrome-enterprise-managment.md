---
controlled_document: true
title: "Google Chrome Enterprise Management"
description: "Web Browser Management - Chrome Enterprise: web browser management, configuration, patching, web browser policies, web browser health"
---

## What is Chrome Enterprise Browser Management?

[Chrome Enterprise Browser Management](https://chromeenterprise.google/browser/management/) is a service that allows IT admins to effectively manage, report, secure, and improve the end user experience with Google Chrome.

## Why Chrome Enterprise?

Chrome Enterprise allows IT administrators a way to build a quick and effective patching policy for outdated or vulnerable Chrome browsers. Additionally, it allows management of Chrome extensions including version information and creation of extension allowlists and blocklists. [Chrome policies](https://chromeenterprise.google/policies/) can also be created and enforced.


## How changes are made

IT admins will propose and create a change in a [CR](https://gitlab.com/gitlab-com/business-technology/change-management). The Change is Alpha tested on a sampling of test machines. The Change is proposed and pushed to the [IT Security Beta Testers](/handbook/business-technology/it/security/) for broader testing and feedback collection. After the final testing is finished, Changes are announced and pushed out to all managed endpoints.
